# IW4x-ModRepo

A repository for IW4x modifications.

### Read **[HOWTOSUBMIT.md](https://gitgud.io/IW4x/iw4x-modrepo/blob/master/HOWTOSUBMIT.md)** for important information on creating and submitting mods!